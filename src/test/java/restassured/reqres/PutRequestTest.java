package restassured.reqres;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class PutRequestTest {
	
	// @author Aditya Garg
	// https://reqres.in/api
	
	@Test
	public void UpdateUserDetails()
	{   
		
		RestAssured.baseURI = "https://reqres.in/api/users/2";
		RequestSpecification httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
	
		httpRequest.header("Content-Type", "application/json");
		requestParams.put("name", "Jay Dean"); // Cast
		requestParams.put("job", "Engineer");
	
		httpRequest.body(requestParams.toJSONString());
		Response response = httpRequest.put();
	 
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		System.out.println(response.body().asString());
	}

}