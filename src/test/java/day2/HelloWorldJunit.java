package day2;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HelloWorldJunit {

	// before method
	@Before
	public void setUp() throws Exception {
		
		System.out.println("inside setup");
	}

	// actual test
	@Test
	public void test() {
		
		// fail("Not yet implemented");
		System.out.println("inside test");

	}

	
	// after
	@After
	public void tearDown() throws Exception {
		
		System.out.println("inside teardown");

	}


	
}
