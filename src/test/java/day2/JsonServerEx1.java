package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JsonServerEx1 {
	
	// member variables..
	
	// variable here - it is accessible across the methods.
	
	int environmentID; //member variable


	@Before
	public void setUp() throws Exception {

		RestAssured.baseURI = "http://localhost:3000";

	}

	@Test
	public void test() {

		getEmployees();
		createEmployee();
		deleteEmployee();

	}

	public void createEmployee() {

		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();
		
		// step 1 a - add header
		
		httpRequest.header("Content-Type", "application/json");

		// step 2 creating a json object

		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "AdityaRestAssured"); // Key Value here
		requestParams.put("salary", "10000"); // Key Value here

		String jsonString = requestParams.toJSONString();

		System.out.println(jsonString);

		// step 2a - adding the json struture to the request body
		httpRequest.body(jsonString ); // attach json string to body

		// step 3 - getting the response - by sending a POST request.
		
		Response response = httpRequest.post("/employees");

		// Get the status code from the Response. In case of 
		// a successfull interaction with the web service, we
		// should get a status code of 200.

		//step 3a - getting the status code
		int statusCode = response.getStatusCode();


		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 201 /*expected value*/, "Correct status code returned");

		// print the response on the console
		response.prettyPrint(); // it will print on console in a good looking format
		
		// extract the id
		JsonPath jsonData = new JsonPath(response.body().asString()); 
		
		int id = jsonData.get("id");
		String name = jsonData.get("name");
		String salary = jsonData.get("salary");
		
		System.out.println("New id = " + id);
		System.out.println("New name = " + name);
		System.out.println("New salary = " + salary);
		
		environmentID = id;

	}

	
	
	public void deleteEmployee() {

		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();
		
		// step 1 a - add header
		
		//httpRequest.header("Content-Type", "application/json");


		// step 3 - getting the response - by sending a POST request.
		
		String endpoint = "/employees/" + environmentID; 
		
		Response response = httpRequest.delete(endpoint);

		// Get the status code from the Response. In case of 
		// a successfull interaction with the web service, we
		// should get a status code of 200.

		//step 3a - getting the status code
		int statusCode = response.getStatusCode();


		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");

		// print the response on the console
		response.prettyPrint(); // it will print on console in a good looking format
		

	}
	
	public void getEmployees() {

		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();

		// step 3 - getting the response
		Response response = httpRequest.get("/employees");

		// Get the status code from the Response. In case of 
		// a successfull interaction with the web service, we
		// should get a status code of 200.

		//step 3a - getting the status code
		int statusCode = response.getStatusCode();


		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");

		// print the response on the console
		response.prettyPrint();


		// var jsonData = pm.response.json();
		JsonPath jsonData = new JsonPath(response.body().asString()); 

		// student id's are integers
		List<String> allNames =jsonData.get("name");

		boolean flagNameFound = false;

		for (int i = 0 ; i < allNames.size(); i++) {

			String name = allNames.get(i);

			if (name.equals("Anand"))
			{
				// id 2 found
				flagNameFound = true;
			}

		}

		System.out.println("Name Anand found = " + flagNameFound);
	}


	@After
	public void tearDown() throws Exception {
	}


}
