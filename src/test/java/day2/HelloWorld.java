package day2;
// sample comment

import org.json.simple.JSONObject;

public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Hello World"); 
		
		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "Aditya"); // Key Value here
		requestParams.put("salary", "10000"); // Key Value here
		
		String jsonString = requestParams.toJSONString();
		
		System.out.println(jsonString);
		
	}

}
