package day2;

import static org.junit.Assert.*;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.DataReaders;

public class RocketChatExercise2DataDriven {
	
	String envauthToken;
	String envuserID;
	String envroomId ;
	String envmessageId ;


	@Before
	public void setUp() throws Exception {
		
		RestAssured.baseURI = "http://101.53.157.237";

	}


	@Test
	public void test() throws InterruptedException, IOException {
		

		login();
		
		// in a for loop 
		// read a excel file - 
		String fileName = "src/test/java/day2/Messages.xlsx";
		String sheetName = "Sheet1";
		
		String [][] data = DataReaders.getExcelDataUsingPoi(fileName, sheetName);
		
		int length = data.length;
		
		System.out.println("length = " + length);
		
		for (int i = 0 ; i < length ; i++) {
			
			System.out.println(data[i][0]);
			
			String messageRead = data[i][0];
			
			postMessage(messageRead);
			// add a delay
			Thread.sleep(3000);
			deleteMessage();
			
		}
		
		
		//postMessage();
		// add a delay
		//Thread.sleep(3000);
		//deleteMessage();

	
	}

	
	public void login() {

		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();
		
		// step 1 a - add header
		
		httpRequest.header("Content-Type", "application/json");

		// step 2 creating a json object

		JSONObject requestParams = new JSONObject();

		//{     "user": "password", 
		// "password": "test123" 
		requestParams.put("user", "mumbai"); // Key Value here
		requestParams.put("password", "test123"); // Key Value here

		String jsonString = requestParams.toJSONString();

		System.out.println(jsonString);

		// step 2a - adding the json struture to the request body
		httpRequest.body(jsonString ); // attach json string to body

		// step 3 - getting the response - by sending a POST request.
		
		Response response = httpRequest.post("/api/v1/login");

		// Get the status code from the Response. In case of 
		// a successfull interaction with the web service, we
		// should get a status code of 200.

		// print the response on the console
		response.prettyPrint(); // it will print on console in a good looking format

		//step 3a - getting the status code
		int statusCode = response.getStatusCode();

		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");

		
		// extract the id
		JsonPath jsonData = new JsonPath(response.body().asString()); 
		
		String userId = jsonData.get("data.userId");
		String authToken = jsonData.get("data.authToken");
		/*
	"rid": "eXFHLuKdMnLHKD9mL",
        "_id": "dx5FgykaW3bWrqbQa",
		 */
		System.out.println("authToken = " + authToken);
		System.out.println("userID = " + userId);
		
		envauthToken = authToken;
		envuserID = userId;

	}
	
	public void postMessage(String message) {
		
		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();
		
		// step 1 a - add header
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.header("X-Auth-Token", envauthToken);
		httpRequest.header("X-User-Id", envuserID);

		// step 2 creating a json object

		JSONObject requestParams = new JSONObject();

		//{ "channel": "#cpwstaug2020", "text": "{{message_data_var}}" }
		requestParams.put("channel", "#cpwstaug2020"); // Key Value here
		requestParams.put("text", message); // Key Value here

		String jsonString = requestParams.toJSONString();

		System.out.println(jsonString);

		// step 2a - adding the json struture to the request body
		httpRequest.body(jsonString ); // attach json string to body

		// step 3 - getting the response - by sending a POST request.
		
		Response response = httpRequest.post("/api/v1/chat.postMessage");

		// Get the status code from the Response. In case of 
		// a successfull interaction with the web service, we
		// should get a status code of 200.

		// print the response on the console
		response.prettyPrint(); // it will print on console in a good looking format

		//step 3a - getting the status code
		int statusCode = response.getStatusCode();

		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");

		
		// extract the id
		JsonPath jsonData = new JsonPath(response.body().asString()); 
		
		String roomId = jsonData.get("message.rid");
		String messageId = jsonData.get("message._id");
		/*
	"rid": "eXFHLuKdMnLHKD9mL",
        "_id": "dx5FgykaW3bWrqbQa",
		 */
		System.out.println("roomId = " + roomId);
		System.out.println("messageId = " + messageId);
		
		envroomId = roomId;
		envmessageId = messageId;

	}
	
	public void deleteMessage() {
		
		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();
		
		// step 1 a - add header
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.header("X-Auth-Token", envauthToken);
		httpRequest.header("X-User-Id", envuserID);

		// step 2 creating a json object

		JSONObject requestParams = new JSONObject();

		//{ 	"roomId": "{{roomid}}", 
		//"msgId": "{{messageid}}", 
		//"asUser": true
		
		requestParams.put("roomId", envroomId); // Key Value here
		requestParams.put("msgId", envmessageId); // Key Value here
		requestParams.put("asUser", true); // Key Value here

		String jsonString = requestParams.toJSONString();

		System.out.println(jsonString);

		// step 2a - adding the json struture to the request body
		httpRequest.body(jsonString ); // attach json string to body

		// step 3 - getting the response - by sending a POST request.
		
		Response response = httpRequest.post("/api/v1/chat.delete");
		
		// print the response on the console
		response.prettyPrint(); // it will print on console in a good looking format

	}

	

	@After
	public void tearDown() throws Exception {
	}

}
