package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StudentExercise {

	@Before
	public void setUp() throws Exception {
		
		RestAssured.baseURI = "http://34.67.191.107:8080/";

	}

	@Test
	public void test() {

		// getStudents
		
		getStudents();
		
	}

	public void getStudents() {
		
		// step 1 - constructing a request
		RequestSpecification httpRequest = RestAssured.given();

		// step 3 - getting the response
		Response response = httpRequest.get("/students/all");

		// Get the status code from the Response. In case of 
		// a successfull interaction with the web service, we
		// should get a status code of 200.

		//step 3a - getting the status code
		int statusCode = response.getStatusCode();


		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");

		// print the response on the console
		//response.prettyPrint();


		// var jsonData = pm.response.json();
		JsonPath jsonData = new JsonPath(response.body().asString()); 

		// student id's are integers
		List<Integer> allId =jsonData.get("studentId");
		List<String> allNames =jsonData.get("firstName");
		List<Integer> allYears =jsonData.get("dateOfBirth.year");
		

		for (int i = 0 ; i < allId.size(); i++) {

			Integer id = allId.get(i);
			String name = allNames.get(i);
			Integer year = allYears.get(i);
			
			System.out.print("Studend id = " + id + ": Student Name = " + name + ": Student Dob Year = " + year);
			System.out.println();
		}

		
	}
	
	@After
	public void tearDown() throws Exception {
	}


}
